# Client-Server communication

## Local running

```
docker compose build
docker compose up -d worker
```

### Attach to logs of the worker

```
docker compose logs -f worker
```

### Example commands to play with app

```
docker compose run --rm client

./cmd.py  # help
./cmd.py getAllItems
./cmd.py addItem "test"
./cmd.py getAllItems
./cmd.py getItem "<printed id>"

```

## Requirements

- Python 3.9.9
