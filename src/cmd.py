#!/usr/bin/env python3
import click
from server import tasks


@click.group()
def main():
    pass


@main.command("addItem", help="add new item")
@click.argument("item", required=True)
def add_item(item):
    tasks.add_item.delay(item)


@main.command("removeItem", help="remove item by id")
@click.argument("item_id", required=True)
def remove_item(item_id):
    tasks.remove_item.delay(item_id)


@main.command("getItem", help="get item by id")
@click.argument("item_id", required=True)
def get_item(item_id):
    tasks.get_item.delay(item_id)


@main.command("getAllItems", help="get all available ids")
def get_all_items():
    tasks.get_all_items.delay()


if __name__ == "__main__":
    main()
