import os

imports = ("server.tasks",)

broker_url = os.getenv("CELERY_BROKER_URL", "amqp://")
result_backend = os.getenv("CELERY_RESULT_BACKEND", "redis://")
