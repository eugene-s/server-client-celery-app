from celery import current_app
from redis import Redis

from kombu.utils.uuid import uuid

__all__ = ("backend",)


class Backend:
    """Data backend class."""

    PREFIX = "data"

    @staticmethod
    def generate_id():
        return uuid()

    @property
    def client(self) -> Redis:
        return current_app.backend.client

    def add_item(self, message: str):
        index = self.generate_id()
        self._set_item(index, message)
        return index

    def _set_item(self, index, message):
        self.client.set(self._internal_index(index), message)

    def remove_item(self, index: str):
        self._remove_item(index)

    def _remove_item(self, index: str):
        self.client.delete(self._internal_index(index))

    def get_item(self, index: str):
        return self._get_item(index)

    def _get_item(self, index):
        return self.client.get(self._internal_index(index)).decode()

    def get_all_items(self):
        return dict(self._get_all_items())

    def _get_all_items(self):
        for key in self._get_all_keys():
            key = self._public_index(key.decode())
            yield (key, self._get_item(key))

    def _get_all_keys(self):
        yield from self.client.scan_iter(f"{self.PREFIX}:*")

    def _internal_index(self, index: str):
        return f"{self.PREFIX}:{index}"

    def _public_index(self, index: str):
        return index.rsplit(":")[-1]


backend = Backend()
