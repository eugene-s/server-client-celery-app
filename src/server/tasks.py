import logging

from celery import current_app as app
from .backend import backend

logger = logging.getLogger(__name__)


@app.task
def add_item(message: str):
    index = backend.add_item(message)
    print(index)
    logger.info("Added new item %s with index %s", message, index)


@app.task
def remove_item(index: str):
    backend.remove_item(index)
    logger.info("Removed item with index %s", index)


@app.task
def get_item(index: str):
    print(backend.get_item(index))
    logger.info("Printed the item with index %s", index)


@app.task
def get_all_items():
    print(backend.get_all_items())
    logging.info("Printed all items.")
