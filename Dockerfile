FROM python:3.9.9-slim

RUN apt-get update -q && \
    apt-get install -y tini

WORKDIR /app

COPY requirements.txt /app/

RUN pip install --no-cache -r requirements.txt

COPY src .

CMD ["/usr/bin/tini", "--", "celery", "worker", "-l", "info"]
